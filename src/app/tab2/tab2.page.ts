import { Component, Injector } from '@angular/core';
import { DestinatarioService, TransferenciaService } from '../services';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page extends BasePage {

  rut:string;
  token:string;
  searchLoad:boolean = false;
  isSaving:boolean = false;
  destinatario:any;
  monto:number;

  constructor(injector: Injector, private dest: DestinatarioService, private trans: TransferenciaService) {

    super(injector);
    this.token = localStorage.getItem('token');
  }

  getDestinatario(){

    this.searchLoad = true;

    this.dest.getDestinatario(this.token, this.rut).subscribe(
      res => {
        this.destinatario = res.destinatario[0];
        console.log(this.destinatario);

        if(this.destinatario == undefined){
          this.showToast("Destinatario no encontrado");
        }

        this.searchLoad = false;
      },
      err => {
        this.searchLoad = false;
        this.getCatch(err);
      }
    );
  }

  setTransferencia(){

    if(this.monto <= 0 || this.monto == undefined){
      this.showToast("el monto debe ser mayor a 0");
    }else if(!this.destinatario){
      this.showToast("destinatario no valido");
    }else{

      this.isSaving = true;

      var data = {
        nombre: this.destinatario['nombre'],
        rut: this.destinatario['rut'],
        banco: this.destinatario['banco'],
        tipo_cuenta: this.destinatario['tipo_cuenta'],
        monto: this.monto
      }

      this.trans.setTransferencia(this.token, data).subscribe(
        res => {
          console.log(res);
          this.showToast("transferencia exitosa");
          this.rut = undefined;
          this.monto = undefined;
          this.destinatario = undefined;

          this.isSaving = false;
        },
        err => {
          this.isSaving = false;
          this.getCatch(err);
        }
      );

    }

  }

}
