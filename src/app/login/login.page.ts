import { Component, Injector } from '@angular/core';
import { AuthenticationService } from '../services';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage {

  usuario:string;
  clave:string;
  load:boolean = false;

  constructor(injector: Injector, private authentication:AuthenticationService) {
    super(injector);
  }

  login(){
    if(this.usuario != undefined && this.clave != undefined){

      this.authentication.login(this.usuario, this.clave).subscribe(
        res => {
          console.log(res);

          if(res.ok == true){
            this.showToast(`Bienvenido ${res.usuario.nombre}`);
            this.router.navigate(['/tabs/tab1']);
          }

          this.load = false;
        },
        e => {
          this.load = false;
          console.log(e);
          if(e.error.err.message){
            return this.showToast(e.error.err.message);
          }
  
          this.getCatch(e);
        }
      );

    }else{
      this.showToast(`Faltan datos por llenar`);
    }
  }

}
