import { Component, Injector } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { DestinatarioService } from '../services';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page extends BasePage {

  public form: FormGroup;
  public isSaving: boolean;
  public searchLoad: boolean;
  token:string;

  public bancos = [];

  public tipos = [{ "title":"Cuenta Vista" },
                  { "title":"Cuenta Corriente" },
                  { "title":"Cuenta Ahorro" }]

  constructor(injector: Injector, private dest: DestinatarioService) {
    super(injector);
    this.token = localStorage.getItem('token');  
  }

  async ngOnInit() {
    this.setupForm();
    await this.getBancos();
  }

  async setupForm() {

    this.form = new FormGroup({
      rut: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required),
      correo: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      banco: new FormControl('', Validators.required),
      tipo: new FormControl('', Validators.required),
      cuenta: new FormControl(null, Validators.required)
    });
    
  }

  getBancos(){
    this.searchLoad = true;

    this.dest.getBancos().subscribe(
      res => {
        this.bancos = res.banks;
        this.searchLoad = false;
      },
      err => {
        this.searchLoad = false;
        this.getCatch(err);
      }
    );
  }


  onSubmit(){

    //Valida le formulario
    if (this.form.invalid) {
      return this.showToast('Formulario no valido');
    }
  
    this.isSaving = true;

    //Obtiene datos del formulario
    const formData = Object.assign({}, this.form.value);

    var data = {
      nombre: formData.nombre,
      rut: formData.rut,
      correo: formData.correo,
      telefono: formData.telefono,
      banco: formData.banco,
      tipo_cuenta: formData.tipo,
      num_cuenta: formData.cuenta
    };

    this.dest.setDestinatario(this.token, data).subscribe(
      res => {
        console.log(res);

        this.showToast("Nuevo destinatario creado");
        this.form.reset();
        this.isSaving = false;
      },
      e => {
        this.isSaving = false;

        console.log(e);

        if(e.error.ok == false){
          return this.showToast(e.error.err.message);
        }

        this.getCatch(e);
      }
    )

  }

  

}
