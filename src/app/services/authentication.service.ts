import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { throwError  } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private api: string = environment.api;
    private headers = environment.httpHeaders;

    private httpOptions = {
        headers: new HttpHeaders(this.headers)
    }
    
    constructor(private http: HttpClient) { }

    login(username: string, password: string){
        
        return this.http.post<any>(`${this.api}/api/login`, { email: username, password: password }, this.httpOptions)
        .pipe(map(user => {

            if (user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('token', user.token);
            }

             return user;
        }));

    }


     // Error handling 
     handleError(error) {
        //let errorMessage = '';
        if(error.error instanceof ErrorEvent) {
            // Get client-side error
            error.message = error.error.message;
        } else {
            // Get server-side error
            error.message = error.error.error;
        }
        
        return throwError(error);
    }

}