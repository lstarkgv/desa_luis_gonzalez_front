import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { throwError  } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DestinatarioService {

  private api:string = environment.api;
  private headers = environment.httpHeaders;

  private httpOptions = {
      headers: new HttpHeaders(this.headers)
  }

  constructor(private http: HttpClient) { }

  getDestinatario(token: string, rut: string){

    this.headers['Authorization'] = `${token}`;
    this.httpOptions.headers = new HttpHeaders(this.headers);

     return this.http.get<any>(`${this.api}/api/destinatario/${rut}`, this.httpOptions)
     .pipe(
          retry(1),
          catchError(this.handleError)
      )

  }

  setDestinatario(token:string, data:object){

    this.headers['Authorization'] = `${token}`;
    this.httpOptions.headers = new HttpHeaders(this.headers);

     return this.http.post<any>(`${this.api}/api/destinatario`, data, this.httpOptions)
     .pipe(
          retry(0),
          catchError(this.handleError)
      )

  }

  getBancos(){

    return this.http.get<any>(`https://bast.dev/api/banks.php`, this.httpOptions)
     .pipe(
          retry(1),
          catchError(this.handleError)
      )

  }

  // Error handling 
  handleError(error) {
      //let errorMessage = '';
      if(error.error instanceof ErrorEvent) {
          // Get client-side error
          error.message = error.error.message;
      } else {
          // Get server-side error
          error.message = error.error.error;
      }
      
      return throwError(error);
  }

}
