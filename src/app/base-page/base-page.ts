import { Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { 
  LoadingController, 
  NavController, 
  ToastController, 
  ModalController, 
  AlertController, 
  IonRefresher, 
  IonInfiniteScroll, 
  Platform
} from '@ionic/angular';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { AuthenticationService } from '../services';

export abstract class BasePage {

  public isErrorViewVisible: boolean;
  public isEmptyViewVisible: boolean;
  public isContentViewVisible: boolean;
  public isLoadingViewVisible: boolean;
  public isAuthViewVisible: boolean;

  protected activatedRoute: ActivatedRoute;
  protected router: Router;
  private title: Title;

  protected refresher: IonRefresher;
  protected infiniteScroll: IonInfiniteScroll;
  protected sanitizer: DomSanitizer;
  protected modalCtrl: ModalController;

  private loader: any;
  private navCtrl: NavController;
  private toastCtrl: ToastController;
  private loadingCtrl: LoadingController;
  private alertCtrl: AlertController;
  private platform: Platform;

  protected auth: AuthenticationService;

  constructor(injector: Injector) {
    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.navCtrl = injector.get(NavController);
    this.loadingCtrl = injector.get(LoadingController);
    this.toastCtrl = injector.get(ToastController);
    this.alertCtrl = injector.get(AlertController);
    this.sanitizer = injector.get(DomSanitizer);
    this.modalCtrl = injector.get(ModalController);
    this.platform = injector.get(Platform);

    this.title = injector.get(Title);
    this.auth = injector.get(AuthenticationService);
  }

  public get api(): string {
    return environment.api;
  }

  public setPageTitle(title: string): void {
    this.title.setTitle(title);
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  public getCatch(err) {
    if(err.error.code === 209){
      window.dispatchEvent(new CustomEvent('user:autologout'));
    }else{
      if(err.error.message){
        this.showToast(err.error.message);
      }else{
        this.showToast("error de red");
      }
    }
  }

  async showLoadingView(params: { showOverlay: boolean }) {

    if (params.showOverlay) {
      const loadingText = 'Cargando...';

      this.loader = await this.loadingCtrl.create({
        message: loadingText
      });
  
      return await this.loader.present();

    } else {

      this.isAuthViewVisible = false;
      this.isErrorViewVisible = false;
      this.isEmptyViewVisible = false;
      this.isContentViewVisible = false;
      this.isLoadingViewVisible = true;
    }

    return true;
  }

  async dismissLoadingView() {
    if (!this.loader) return;

    try {
      return await this.loader.dismiss()
    } catch (error) {
      console.log('ERROR: LoadingController dismiss', error);
    }
  }

  showContentView() {

    this.isAuthViewVisible = false;
    this.isErrorViewVisible = false;
    this.isEmptyViewVisible = false;
    this.isLoadingViewVisible = false;
    this.isContentViewVisible = true;

    this.dismissLoadingView();
  }

  showEmptyView() {

    this.isAuthViewVisible = false;
    this.isErrorViewVisible = false;
    this.isLoadingViewVisible = false;
    this.isContentViewVisible = false;
    this.isEmptyViewVisible = true;

    this.dismissLoadingView();
  }

  showErrorView() {

    this.isAuthViewVisible = false;
    this.isLoadingViewVisible = false;
    this.isContentViewVisible = false;
    this.isEmptyViewVisible = false;
    this.isErrorViewVisible = true;

    this.dismissLoadingView();
  }

  showAuthView() {
    
    this.isLoadingViewVisible = false;
    this.isContentViewVisible = false;
    this.isEmptyViewVisible = false;
    this.isErrorViewVisible = false;
    this.isAuthViewVisible = true;

    this.dismissLoadingView();
  }

  onRefreshComplete(data = null) {

    if (this.refresher) {
      this.refresher.disabled = true;
      this.refresher.complete();
      setTimeout(() => {
        this.refresher.disabled = false;
      }, 100);
    }

    if (this.infiniteScroll) {
      this.infiniteScroll.complete();

      if (data && data.length === 0) {
        this.infiniteScroll.disabled = true;
      } else {
        this.infiniteScroll.disabled = false;
      }
    }
  }

  async showToast(message: string) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    return await toast.present();
  }

  async showAlert(message: string) {
  
    const okText = 'OK';

    const alert = await this.alertCtrl.create({
      header: '',
      message: message,
      buttons: [okText]
    });

    return await alert.present();
  }

  showConfirm(message: string): Promise<any> {

    return new Promise(async (resolve) => {

      const str = ['OK', 'CANCEL'];

      const confirm = await this.alertCtrl.create({
        header: '',
        message: message,
        buttons: [{
          text: 'CANCEL',
          role: 'cancel',
          handler: () => resolve(false),
        }, {
          text: 'OK',
          handler: () => resolve(true)
        }]
      });

      confirm.present();
      
    });

  }


  isDesktop(): boolean {
    return this.platform.is('desktop');
  }

  isIos(): boolean {
    return this.platform.is('ios');
  }

  isAndroid(): boolean {
    return this.platform.is('android');
  }

  isHybrid(): boolean {
    return this.platform.is('hybrid');
  }

  isPwa(): boolean {
    return this.platform.is('pwa');
  }

  isMobile(): boolean {
    return this.platform.is('mobile');
  }

  setRoot(url: string) {
    this.navCtrl.setDirection('root', false);
    this.router.navigateByUrl(url);
  }

  setRelativeRoot(page: string, queryParams: any = {}) {
    this.navCtrl.setDirection('root', false);
    return this.router.navigate([page], {
      queryParams: queryParams,
      relativeTo: this.activatedRoute
    });
  }

  navigateTo(page: any, queryParams: any = {}) {
    return this.router.navigate([page], { queryParams: queryParams });
  }

  navigateToRelative(page: any, queryParams: any = {}) {
    return this.router.navigate([page], {
      queryParams: queryParams,
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });
  }

  goBack() {
    this.navCtrl.back();
  }

  getParams() {
    return this.activatedRoute.snapshot.params;
  }

  getQueryParams() {
    return this.activatedRoute.snapshot.queryParams;
  }

  goToCartPage() {
    this.navigateTo('/1/cart');
  }

}
