import { Component, Injector, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { BasePage } from '../base-page/base-page';
import { TransferenciaService } from '../services';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page extends BasePage{

  columns = [];
  rows = [];
  temp;
  loadingIndicator = false;
  headers = [];
  token:string;

  ColumnMode = ColumnMode;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  constructor(injector: Injector, private trans: TransferenciaService) {
  
    super(injector);
    this.token = localStorage.getItem('token');
  }

  ngOnInit(){
    this.getTransferencias();
  }

  getTransferencias(){

    this.rows = [];
    this.loadingIndicator = true;

    this.trans.getTransferencias(this.token).subscribe(
      res => {
        var content = res.transferencias;

        this.headers = Object.keys(content[0]);
        this.columns = this.getColumns(this.headers);
        this.temp = [...content];
        this.rows = content;
        console.log(content)

        this.loadingIndicator = false;
      },
      err => {
        this.loadingIndicator = false;
        this.getCatch(err);
      }
    );

  }

  getColumns(headers){

    let columnas = [];

    headers.forEach(head => {

      let element = {
        name : head,
        prop: head
      }
      columnas.push(element);

    });

    return columnas;

  }

  getCatch(err) {
    if(err.error.code === 209){
      window.dispatchEvent(new CustomEvent('user:autologout'));
    }else{
      if(err.error.message){
        this.showToast(err.error.message);
      }else{
        this.showToast("error de red");
      }
    }
  }



}
