FROM node:12.9.1-buster-slim

RUN mkdir desafio

WORKDIR /desafio

COPY package*.json ./

RUN npm install
RUN npm install -g @angular/cli@10.0.8

COPY . .

RUN sh -c "ng build --prod"

EXPOSE 8100:8100

CMD ["node", "server.js" ]
