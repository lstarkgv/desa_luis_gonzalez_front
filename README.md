# Desafío Tecnico - Luis González Villagra

Pieza de software que simula una transferencia con las siguientes funcionalidades:

 - Nuevo Destinatario
 - Transferencia
 - Historial

Como extra se agrego sistema de seguridad basado en token (JWT).

 * Credenciales:
  
   **usuario**: admin@banco.com
   **clave**: adminripley

# Arquitectura 

* El FrontEnd esta creado con Angular 12 e Ionic (para UI responsiva compatible con todas las pantallas y temas oscuros si este esta activo).

* El BackEnd es un API REST creado con NodeJS Express con el ORM Mongoose para conectar con MongoDB

* Base de datos MongoDB Cluster bajo MongoDB Atlas (SaaS en Google Cloud Platform).

* Infraestructura montada en GCP y desplegado con Docker en servidor Ubuntu Server.

**Repositorio Backend:** https://gitlab.com/lstarkgv/desafio_luis_gonzalez

**Repositorio Frontend:** https://gitlab.com/lstarkgv/desa_luis_gonzalez_front

![arquitectura](arquitectura.png)


