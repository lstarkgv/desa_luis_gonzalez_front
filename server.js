const express = require('express');
const exec = require('child_process').exec;
const path = require('path');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, 'www')));

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	next();
});

app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, 'www/index.html'));
});

app.listen(process.env.PORT || 8100, () => {
    console.log('Escuchando puerto: ', process.env.PORT || 8100);
});
